/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var authType = function(context, options) {
        var auth = this;
        auth.context = context;
        auth.initOptions = options;
        auth.conf = auth.initOptions.conf;
    };

    authType.loadAuth = function(context, options) {
        var dr = $.Deferred();

        // load the auth driver code
        var authConf = options.authConf;
        requirejs([authConf["sa:driver"]], function(specializedAuthType) {
            var auth = new authType(context, {
                conf: authConf["sa:conf"]
            });
            var specializedAuth = new specializedAuthType();
            $.extend(auth, specializedAuth);
            auth.init().done(function() {
                dr.resolve(auth);
            }).fail(drff(dr));
        });

        return dr.promise();
    };

    authType.prototype.getIRI = function() {
        return this.conf["sa:iri"];
    };

    authType.prototype.getAttributes = function() {
        var attributes = {
            "sw:Auth": true
        };
        if (this.getSpecificAttributes) {
            $.extend(attributes, this.getSpecificAttributes(), attributes);
        }
        return attributes;
    };

    authType.prototype.getAuthInfo = function(options) {
        var dr = $.Deferred();
        var auth = this;

        if (this.isKitchenLocal() || !auth.getAuthInfoSpecialized) {
            dr.resolve();
        } else {
            dr.resolve(auth.getAuthInfoSpecialized());
        }

        return dr.promise();
    };

    authType.prototype.isKitchenLocal = function() {
        var kitchenLocal = this.conf["sa:kitchenLocal"];
        if (kitchenLocal === undefined) {
            kitchenLocal = true;
        }
        return kitchenLocal;
    };

    /**
     * @param options
     *   @message the message to be processed.
     */
    authType.prototype.onMessage = function(options) {
        var dr = $.Deferred();
        var auth = this;
        var msg = options.message;

        if (msg["@type"] == "sm:task") {
            if (msg["sm:taskType"] == "sm:getAuthInfo") {
                if (auth.getAuthInfo) {
                    auth.getAuthInfo(msg["sm:taskParameters"]).done(function(authInfo) {
                        dr.resolve(authInfo);
                    }).fail(drff(dr));
                }
                else {
                    dr.resolve();
                }
            }
            else if (msg["sm:taskType"] == "sm:login") {
                if (auth.login) {
                    auth.login(msg["sm:taskParameters"]).done(drdf(dr)).fail(drff(dr));
                }
                else {
                    dr.resolve();
                }
            }
            else if (msg["sm:taskType"] == "sm:logout") {
                if (auth.logout) {
                    auth.logout(msg["sm:taskParameters"]).done(drdf(dr)).fail(drff(dr));
                }
                else {
                    dr.resolve();
                }
            }
            else {
                dr.resolve();
            }
        }
        else {
            dr.resolve();
        }

        return dr.promise();
    };

    return authType;
});
